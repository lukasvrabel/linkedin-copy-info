document.getElementById("test").addEventListener('click', () => {
    console.log("Popup DOM fully loaded and parsed");

    

    function modifyDOM() {
        function copyTextToClipboard(text) {
            var copyFrom = document.createElement("textarea");
            copyFrom.textContent = text;
            var body = document.getElementsByTagName('body')[0];
            body.appendChild(copyFrom);
            copyFrom.select();
            document.execCommand('copy');
            body.removeChild(copyFrom);
        }

        //You can play with your DOM here or check URL against your regex
        console.log('Tab script:');
        console.log(document.getElementsByClassName('pv-contact-info__contact-type ci-email').length)
        var emailVisible = (document.getElementsByClassName('pv-contact-info__contact-type ci-email').length > 0)
        console.log('email visible: ' + emailVisible);
        if (!emailVisible) {
            console.log('clicking')
            document.getElementsByClassName('contact-see-more-less')[0].click();
        }

        var res = {
          name: document.getElementsByClassName('pv-top-card-section__name')[0].innerHTML,
          url: document.URL,
          email: document.getElementsByClassName('pv-contact-info__contact-type ci-email')[0].getElementsByClassName('pv-contact-info__contact-link')[0].innerHTML.trim(),
        }

        // copyTextToClipboard(res.name + '\t' + res.url + '\t' + res.email);
        // console.log(document.body);
        console.log(res)
        return res; //document.body.innerHTML;
    }

    function copyTextToClipboard(text) {
        var copyFrom = document.createElement("textarea");
        copyFrom.textContent = text;
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(copyFrom);
        copyFrom.select();
        document.execCommand('copy');
        body.removeChild(copyFrom);
    }


    var allResults = []
    chrome.tabs.query({}, function(tabs) {
        for (var i = 0; i < tabs.length; i++) {
            var tabId = tabs[i].id;
            console.log('on tab ' + tabId);
            
            chrome.tabs.executeScript(
                tabId,
                {
                    code: '(' + modifyDOM + ')();' //argument here is a string but function.toString() returns function's code
                }, 
                (results) => {
                    res = results[0]
                    //Here we have just the innerHTML and not DOM structure
                    console.log(res);
                    if (!res) {
                        res = {name: 'reload tab', url: 'x', email: 'x'};
                    }
                    allResults.push(res);
                    // copyTextToClipboard(res.name + '\t' + res.url + '\t' + res.email);
                }
            );        // do what you want    
            
        }
        setTimeout(() => {
            console.log('all results:')
            console.log(allResults)
            resText = ''
            allResults.forEach((res) => {resText += res.name + '\t' + res.url + '\t' + res.email + '\n'})
            console.log(resText);
            copyTextToClipboard(resText)

        }, 500);
    });

    //We have permission to access the activeTab, so we can call chrome.tabs.executeScript:
    chrome.tabs.executeScript({
        code: '(' + modifyDOM + ')();' //argument here is a string but function.toString() returns function's code
    }, (results) => {
        res = results[0]
        //Here we have just the innerHTML and not DOM structure
        console.log('Popup script:')
        console.log(res);
        // copyTextToClipboard(res.name + '\t' + res.url + '\t' + res.email);


    });
});